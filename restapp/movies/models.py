from __future__ import unicode_literals

from django.db import models


class Genre(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Movie(models.Model):
    title = models.CharField(max_length=255)
    genre = models.ForeignKey(Genre)
    year = models.IntegerField()

    def __unicode__(self):
        return self.title
