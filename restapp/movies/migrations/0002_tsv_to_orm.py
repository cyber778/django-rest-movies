# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-05-30 00:51
from __future__ import unicode_literals

import csv

from django.db import migrations

# MOVIE = 0
# YEAR = 1
# GENRE = 2

def load_data(apps, schema_editor):
    Movie = apps.get_model('movies', 'Movie')
    Genre = apps.get_model('movies', 'Genre')
    with open('movies_genres.tsv', 'r') as movies:
        for movie in movies:
            title, year, genre = movie.replace('\n', '').split('\t')
            year = int(year)
            g, created = Genre.objects.get_or_create(name=genre)
            m, created = Movie.objects.get_or_create(genre=g, title=title, year=year)


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_data)
    ]
