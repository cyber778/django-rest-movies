from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from .models import Movie, Genre

class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = '__all__'


class GenreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Genre
        fields = ('name',)


class GenreListSerializer(serializers.ModelSerializer):
    movie_count = SerializerMethodField()

    class Meta:
        model = Genre
        fields = '__all__'

    def get_movie_count(self, obj):
        return Movie.objects.filter(genre=obj).count()