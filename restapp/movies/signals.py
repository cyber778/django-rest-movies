from django.db.models.signals import post_save

from .models import Genre, Movie
from .utils import flush_cache


post_save.connect(flush_cache(), sender=Genre)
post_save.connect(flush_cache(), sender=Movie)