
from rest_framework.views import APIView
from rest_framework.generics import (
    DestroyAPIView,
    CreateAPIView,
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    UpdateAPIView,
)
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from django.shortcuts import get_object_or_404, render

from .models import Movie, Genre
from .serializers import MovieSerializer, GenreSerializer, GenreListSerializer


class MovieListView(ListAPIView):
    serializer_class = MovieSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = Movie.objects.all()
        genre = self.request.GET.get('genre', None)
        if genre:
            queryset_list = queryset_list.filter(genre=genre)
        return queryset_list


class MovieDetailView(RetrieveAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MovieDestroyView(DestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MovieUpdateView(UpdateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MovieUpdateView(RetrieveUpdateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MovieCreateView(CreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class GenreListView(ListAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreListSerializer


class GenreDetailView(RetrieveAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreListSerializer


class GenreDestroyView(DestroyAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class GenreUpdateView(UpdateAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class GenreUpdateView(RetrieveUpdateAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class GenreCreateView(CreateAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


@api_view(['GET'])
def get_sequel_count(request, pk=None):
    if not pk:
        return Response("Bad Request")

    try:
        m = Movie.objects.get(pk=pk)
        name = m.title
        count = Movie.objects.filter(title__contains=name).count()
        return Response({"sequel_count": count})

    except ObjectDoesNotExist:
        return Response("Bad Request")

@api_view(['GET'])
def genre_volume_by_year(request):
    ctx = {}
    years = sorted(Movie.objects.values_list('year', flat=True).distinct())
    for year in years:
        genre, count = Movie.objects.filter(year=year).values_list('genre').annotate(gc=Count('genre__name')).order_by('-gc')[0]
        ctx[year] = {'genre': genre, 'count': count}
    return Response(ctx)


def movies_home(request):
    pass
