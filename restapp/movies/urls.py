from rest_framework.urlpatterns import format_suffix_patterns

from django.conf.urls import url
from django.views.decorators.cache import cache_page

from . import views

urlpatterns = [
    url(r'^movies/$', cache_page(60*60)(views.MovieListView.as_view())),
    url(r'^movies/(?P<pk>\d+)/$', views.MovieDetailView.as_view()),
    url(r'^movies/(?P<pk>\d+)/delete/$', views.MovieDestroyView.as_view()),
    url(r'^movies/(?P<pk>\d+)/update/$', views.MovieUpdateView.as_view()),
    url(r'^movies/create/$', views.MovieCreateView.as_view()),
    url(r'^movies/genre-by-year/$', views.genre_volume_by_year),
    url(r'^movies/(?P<pk>\d+)/sequels/$', views.get_sequel_count),
    # url(r'^movies/count/$', views.MovieCreateView.as_view()),
    url(r'^genres/$', cache_page(60*60)(views.GenreListView.as_view())),
    url(r'^genres/(?P<pk>\d+)/$', views.GenreDetailView.as_view()),
    url(r'^genres/(?P<pk>\d+)/delete/$', views.GenreDestroyView.as_view()),
    url(r'^genres/(?P<pk>\d+)/update/$', views.GenreUpdateView.as_view()),
    url(r'^genres/create/$', views.GenreCreateView.as_view()),
    # url(r'^movies/filter/genre/(?P<pk>\d+)/$', views.MovieFilterGenre.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)