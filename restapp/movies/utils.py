from django.core.cache import cache

def flush_cache():
    '''
    this is a reusable component in case we want to make some custom changes or flush only specific keys
    :return:
    '''
    cache.clear()