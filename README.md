# Django REST Movies #

## Summary ##

just showing off some skills using django 1.9 and django rest framework 3.4.6 for data serialization

API to create, retrieve, list, update, and delete movies and genres
API to filter movies by genre
API to retrieve and list counts of movies by genre

Example for finding movie by genre : /api/movies/?genre=History

Example for finding a specific genre and its movie count: /api/genres/4/

Example to retrieve or list movies that include a "number of sequels": /api/movies/(<pk>)/sequels/

Example for showing all genres: /api/genres

Example for showing all movies: /api/movies

For more suggestions look in the urls